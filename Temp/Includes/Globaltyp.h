/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1544006617_1_
#define _BUR_1544006617_1_

#include <bur/plctypes.h>

/* Datatypes and datatypes of function blocks */
typedef struct ProPosMode_type
{	plcbit wRel;
	plcbit wAbs;
	signed long wTargetPos;
	signed long wTargetPosSend;
	unsigned long wProVlocSend;
	unsigned long wProVloc;
} ProPosMode_type;

typedef struct ProVelMode_type
{	signed long wSetSpd;
	signed long wSetSpdSend;
} ProVelMode_type;

typedef struct ElmoMotor_type
{	plcbit wCINH;
	plcbit wBG;
	plcbit wQSP;
	unsigned short wControlWord;
	signed char wDrvMode;
	struct ProPosMode_type wOpMode1;
	struct ProVelMode_type wOpMode3;
	unsigned short rCINH;
	signed long rActSpeed;
	signed short rActCurrent;
	unsigned char rDrvMode;
	signed long rEncCount;
	unsigned short errCode;
	plcbit rCommOK;
} ElmoMotor_type;

typedef struct Battery_typ
{	unsigned short Voltage;
	signed short Current;
	unsigned short RestCapacity;
	unsigned short NominalCapacity;
	signed short CyclicCount;
	unsigned short Percentage;
	unsigned short ProtectStatus;
} Battery_typ;

typedef struct VCMC_typ
{	unsigned char VCU_Shiftlevel_Req;
	unsigned char VCU_Control_Enable;
	unsigned short VCU_RPM_Req;
	unsigned short VCU_Torque_Req;
	unsigned char VCU_Brake_feedback_Req;
	unsigned char MCU_CheckSum;
} VCMC_typ;

typedef struct MCUStatus1_typ
{	unsigned char MCU_Current_Status;
	plcbit MCU_Energy_Feedback;
	plcbit Drive_Mode;
	unsigned char MCU_Current_Gear;
	unsigned short MCU_Current_Torque;
	unsigned short MCU_Current_RPM;
	unsigned char MCU_Current_Temperature;
	unsigned char MCU_Diag;
	unsigned char MCU_CheckSum;
} MCUStatus1_typ;

typedef struct MCUStatus2_typ
{	unsigned short MCU_Voltage;
	unsigned short MCU_Current;
	unsigned char MCU_CheckSum;
} MCUStatus2_typ;

typedef struct ESP_typ
{	plcbit homing_OK;
	signed long homing_point;
	signed long angle_cmd;
	signed long angle_act;
	signed short angle_limit;
} ESP_typ;

typedef struct MCU_typ
{	unsigned char shift_level_req;
	unsigned char shift_level_act;
	signed short speed_cmd;
	signed short speed_cmd_correct;
	signed short speed_req;
	signed short speed_act;
	signed short speed_limit;
} MCU_typ;

typedef struct Serial_typ
{	unsigned char CommOver;
	unsigned char CommFirst;
	plcbit signal_OK;
	plcbit park;
	plcbit control;
	unsigned char interrupt_count;
} Serial_typ;

typedef struct Gyro_typ
{	signed short Angle;
	signed short AngleSpeed;
	plcbit SignalOK;
} Gyro_typ;

typedef struct Brake_typ
{	plcbit start;
	unsigned char count;
} Brake_typ;

typedef struct Remote_typ
{	plcbit signal_OK;
	unsigned char steer_ch_value;
	unsigned char throttle_ch_value;
	unsigned char direction_ch_value;
	unsigned char stop_ch_value;
	unsigned char steer;
	unsigned char throttle;
	unsigned char direction;
	unsigned char max_value;
	unsigned char min_value;
	unsigned short throttle_max;
	unsigned short steer_max;
	plcbit stop;
	plcbit control;
} Remote_typ;

typedef struct Can_typ
{	plcbit signal_OK;
	plcbit park;
	unsigned char interrupt_count;
} Can_typ;

typedef struct DrivingModel_typ
{	unsigned char RunMode;
	plcbit remote_stop;
	signed short Temprature;
	struct ESP_typ ESP;
	struct MCU_typ MCU;
	struct Serial_typ Serial;
	struct Gyro_typ Gyro;
	struct Brake_typ Brake;
	struct Remote_typ Remote;
	struct Can_typ Can;
} DrivingModel_typ;

typedef struct DOMapping_typ
{	plcbit PowerLed1;
	plcbit PowerLed2;
	plcbit PowerLed3;
	plcbit PowerLed4;
	plcbit StatusLed;
	plcbit LockOff;
	plcbit BrakeEnable;
	plcbit BrakeStart;
	plcbit ModeLed0;
	plcbit ModeLed1;
	plcbit ModeLed2;
} DOMapping_typ;

typedef struct DIMapping_typ
{	plcbit QuickStop;
	plcbit LockOff;
	plcbit HomingInput;
	plcbit RemoteStop;
} DIMapping_typ;

typedef struct ConstModel_typ
{	plcstring ProductModel[12];
	plcstring ProgramVersion[21];
	plcstring ProduceDate[11];
	float BackWheelDiameter;
	float Tread;
	float WheelBase;
	signed long TurningZeroPoint;
	signed long TurningSpeed;
	float LinearRatio;
	unsigned short LineSpeedLimit;
	unsigned short SteerAngleRatio;
	float CorrectK;
	signed short SteerCurrentLimit;
	signed short VCUTempratureLimit;
} ConstModel_typ;

typedef struct SysFault_typ
{	plcbit FaultProduce;
	unsigned short MCU_Fault;
	unsigned short BMS_Fault;
	unsigned short EPS_Fault;
	unsigned short VCU_Fault;
	unsigned char FaultCode[8];
	unsigned char FaultNum;
} SysFault_typ;

typedef struct VCUStatus_typ
{	signed short Temprature;
	plcbit Module2Status;
	plcbit Module3Status;
	plcbit Module4Status;
	plcbit Module5Status;
	plcbit Module6Status;
} VCUStatus_typ;

typedef struct PIDControl_typ
{	signed short SetPoint;
	signed long Proportion;
	float Integral;
	signed long Derivative;
	signed short LastError;
	signed short PrevError;
	signed short SumError;
	signed short Error;
	signed short dError;
	signed long ret;
	signed short NowPoint;
} PIDControl_typ;

typedef struct Storage_typ
{	signed long homing_distance;
	signed short SteerCorrectLeft;
	signed short SteerCorrectRight;
	float SteerAngleLimit;
	signed long TurningSpeed;
	unsigned short LineSpeedLimit;
} Storage_typ;

typedef struct Command_typ
{	signed short remote_speed_cmd;
	signed short remote_steer_cmd;
	signed short pc_speed_cmd;
	signed short pc_steer_cmd;
	signed short can_speed_cmd;
	signed short can_steer_cmd;
} Command_typ;






__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Global.typ\\\" scope \\\"global\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1544006617_1_ */

