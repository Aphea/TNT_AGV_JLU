/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1542334538_10_
#define _BUR_1542334538_10_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
#else
#endif


/* Variables */
_BUR_LOCAL float temp2;
_BUR_LOCAL unsigned char temp;
_BUR_LOCAL unsigned char RecFrame2[8];
_BUR_LOCAL plcbit switchFlag;
_BUR_LOCAL struct CANrd CANrd_1;
_BUR_LOCAL struct CANrd CANrd_0;
_BUR_LOCAL struct CANqueue CANqueue_1;
_BUR_LOCAL struct CANqueue CANqueue_0;
_BUR_LOCAL struct CANquwr CANquwr_0;
_BUR_LOCAL unsigned char RecFrame1[8];
_BUR_LOCAL unsigned char TranFrame[8];





__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Motor/Variables.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CAN_Lib/CAN_Lib.fun\\\" scope \\\"global\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1542334538_10_ */

