/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1542940010_1_
#define _BUR_1542940010_1_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
#else
#endif


/* Variables */
_GLOBAL struct Command_typ Command;
_GLOBAL_RETAIN struct Storage_typ Storage;
_GLOBAL_RETAIN signed long HomingDistance;
_GLOBAL signed short LineAI;
_GLOBAL_RETAIN struct PIDControl_typ PIDControl;
_GLOBAL_RETAIN signed short SteerCorrectRight;
_GLOBAL_RETAIN signed short SteerCorrectLeft;
_GLOBAL struct PIDControl_typ LineSensorPID;
_GLOBAL struct VCUStatus_typ VCUStatus;
_GLOBAL struct SysFault_typ SysFault;
_GLOBAL_RETAIN float Meter;
_GLOBAL_RETAIN unsigned short MileMeter;
_GLOBAL struct ConstModel_typ ConstModel;
_GLOBAL struct DIMapping_typ DIMapping;
_GLOBAL struct DOMapping_typ DOMapping;
_GLOBAL struct DrivingModel_typ DrivingModel;
_GLOBAL struct MCUStatus2_typ MCUStatus2;
_GLOBAL struct MCUStatus1_typ MCUStatus1;
_GLOBAL struct VCMC_typ VCMC;
_GLOBAL struct CANopen CANopen_0;
_GLOBAL plcbit g_bRecOverTime;
_GLOBAL plcbit g_bRS232RecOK;
_GLOBAL struct Battery_typ Battery;
_GLOBAL struct ElmoMotor_type ElmoMotor[2];





__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Global.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CAN_Lib/CAN_Lib.fun\\\" scope \\\"global\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1542940010_1_ */

