/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1543999539_3_
#define _BUR_1543999539_3_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
#else
#endif


/* Variables */
_BUR_LOCAL unsigned char int_count;
_BUR_LOCAL plcbit over;
_BUR_LOCAL unsigned char remote_ch_value[8];
_BUR_LOCAL struct CANrd CANrd_3;
_BUR_LOCAL struct CANqueue CANqueue_3;





__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Remote/Variables.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CAN_Lib/CAN_Lib.fun\\\" scope \\\"global\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1543999539_3_ */

