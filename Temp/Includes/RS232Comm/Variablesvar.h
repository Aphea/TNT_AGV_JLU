/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1543982151_1_
#define _BUR_1543982151_1_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
 #define OverTime 200U
#else
 _LOCAL_CONST unsigned char OverTime;
#endif


/* Variables */
_BUR_LOCAL unsigned short RecIntervals;
_BUR_LOCAL unsigned char CycleCount;
_BUR_LOCAL plctime RecTimeLast;
_BUR_LOCAL plcbit rx_rec_cmd_OK;
_BUR_LOCAL unsigned char ReaptLast;
_BUR_LOCAL plcbit rx_repeat_OK;
_BUR_LOCAL plcbit rx_sum_OK;
_BUR_LOCAL plcbit rx_headtail_OK;
_BUR_LOCAL plcbit rx_rec_OK;
_BUR_LOCAL unsigned char tx_data[24];
_BUR_LOCAL unsigned char rx_data[16];
_BUR_LOCAL struct FRM_rbuf FRM_rbuf_0;
_BUR_LOCAL struct FRM_read FRM_read_0;
_BUR_LOCAL struct FRM_write FRM_write_0;
_BUR_LOCAL struct FRM_gbuf FRM_gbuf_0;
_BUR_LOCAL struct FRM_xopen FRM_xopen_0;
_BUR_LOCAL struct XOPENCONFIG XOPENCONFIG_0;





__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/RS232Comm/Variables.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/dvframe/dvframe.fun\\\" scope \\\"global\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1543982151_1_ */

