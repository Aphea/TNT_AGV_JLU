/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1543999540_5_
#define _BUR_1543999540_5_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
#else
#endif


/* Variables */
_BUR_LOCAL unsigned short UseTime;
_BUR_LOCAL plctime LastTime;
_BUR_LOCAL signed long TurningSpeed;
_BUR_LOCAL float SteerAngleLimit;
_BUR_LOCAL unsigned short LineSpeedLimit;
_BUR_LOCAL unsigned char CAN_cmd_1_sum;
_BUR_LOCAL unsigned char CAN_cmd_0_sum;
_BUR_LOCAL unsigned char Last_id;
_BUR_LOCAL signed short AngleCmd;
_BUR_LOCAL signed short SpeedCmd;
_BUR_LOCAL unsigned char CAN_cmd_1[8];
_BUR_LOCAL unsigned char CAN_cmd_0[8];
_BUR_LOCAL struct CANrd CANrd_3;
_BUR_LOCAL struct CANrd CANrd_2;
_BUR_LOCAL struct CANqueue CANqueue_3;
_BUR_LOCAL struct CANqueue CANqueue_2;
_BUR_LOCAL unsigned char CAN_cycle_count;
_BUR_LOCAL unsigned char CAN_info_1_count;
_BUR_LOCAL unsigned char CAN_info_0_count;
_BUR_LOCAL unsigned char CAN_info_1[8];
_BUR_LOCAL unsigned char CAN_info_0[8];
_BUR_LOCAL struct CANquwr CANquwr_2;
_BUR_LOCAL struct CANquwr CANquwr_1;





__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/CanComm/Variables.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CAN_Lib/CAN_Lib.fun\\\" scope \\\"global\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1543999540_5_ */

