/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1542606421_1_
#define _BUR_1542606421_1_

#include <bur/plctypes.h>

/* Datatypes and datatypes of function blocks */
typedef struct Elmo_typ
{	unsigned char MO;
	signed long PX;
	unsigned char UM;
	signed long PA;
	unsigned long JV;
	signed long VX;
	signed long IQ;
	unsigned long AC;
	unsigned long SP;
} Elmo_typ;






__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Steer/Types.typ\\\" scope \\\"local\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1542606421_1_ */

