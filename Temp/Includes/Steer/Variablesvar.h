/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1542882984_1_
#define _BUR_1542882984_1_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
#else
#endif


/* Variables */
_BUR_LOCAL plcbit set_demand;
_BUR_LOCAL unsigned char set_step;
_BUR_LOCAL unsigned char inq_step;
_BUR_LOCAL signed long target_pos;
_BUR_LOCAL struct Elmo_typ Elmo;
_BUR_LOCAL signed long encount;
_BUR_LOCAL unsigned char RecFrame[8];
_BUR_LOCAL struct CANrd CANrd_2;
_BUR_LOCAL struct CANqueue CANqueue_2;
_BUR_LOCAL unsigned long Elmo_ID;
_BUR_LOCAL unsigned char run_status;
_BUR_LOCAL unsigned char tran_frame[8];
_BUR_LOCAL struct CANquwr CANquwr_1;





__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Steer/Variables.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CAN_Lib/CAN_Lib.fun\\\" scope \\\"global\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1542882984_1_ */

