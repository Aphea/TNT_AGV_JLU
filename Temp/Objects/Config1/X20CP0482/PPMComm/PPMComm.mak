UnmarkedObjectFolder := F:/ProductData/JiLinUniversity/TNT_AGV_JLU_demo2/Logical/PPMComm
MarkedObjectFolder := F:/ProductData/JiLinUniversity/TNT_AGV_JLU_demo2/Logical/PPMComm

$(AS_CPU_PATH)/PPMComm.br: \
	$(AS_PROJECT_CPU_PATH)/Cpu.per \
	FORCE \
	$(AS_CPU_PATH)/PPMComm/PPMComm.ox
	@'$(AS_BIN_PATH)/BR.AS.TaskBuilder.exe' '$(AS_CPU_PATH)/PPMComm/PPMComm.ox' -o '$(AS_CPU_PATH)/PPMComm.br' -v V1.00.0 -f '$(AS_CPU_PATH)/NT.ofs' -offsetLT '$(AS_BINARIES_PATH)/$(AS_CONFIGURATION)/$(AS_PLC)/LT.ofs' -T SG4  -M ARM  -B D4.34 -extConstants -r Cyclic2 -p 5 -s 'PPMComm' -L 'AsCANopen: V*, AsIecCon: V*, astime: V*, brsystem: V*, CAN_Lib: V*, dvframe: V*, LoopCont: V3.10.0, operator: V*, runtime: V*, sys_lib: V*' -P '$(AS_PROJECT_PATH)' -secret '$(AS_PROJECT_PATH)_br.as.taskbuilder.exe'

$(AS_CPU_PATH)/PPMComm/PPMComm.ox: \
	$(AS_CPU_PATH)/PPMComm/a.out \
	FORCE 
	@'$(AS_BIN_PATH)/BR.AS.Backend.exe' '$(AS_CPU_PATH)/PPMComm/a.out' -o '$(AS_CPU_PATH)/PPMComm/PPMComm.ox' -T SG4 -M ARM -r Cyclic2   -G V4.1.2  -B D4.34 -secret '$(AS_PROJECT_PATH)_br.as.backend.exe'

$(AS_CPU_PATH)/PPMComm/a.out: \
	$(AS_CPU_PATH)/PPMComm/Main.c.o \
	$(AS_CPU_PATH)/PPMComm/_bur_pvdef.c.o
	@'$(AS_BIN_PATH)/BR.AS.CCompiler.exe' -link '$(AS_CPU_PATH)/PPMComm/Main.c.o' '$(AS_CPU_PATH)/PPMComm/_bur_pvdef.c.o'  -o '$(AS_CPU_PATH)/PPMComm/a.out'  -G V4.1.2  -T SG4  -M ARM  '-Wl,$(AS_PROJECT_PATH)/Logical/Libraries/LoopCont/SG4/Arm/libLoopCont.a' '-Wl,$(AS_SYSTEM_PATH)/D0434/SG4/ARM/libbrsystem.a' '-Wl,$(AS_SYSTEM_PATH)/D0434/SG4/ARM/libsys_lib.a' '-Wl,$(AS_SYSTEM_PATH)/D0434/SG4/ARM/libAsCANopen.a' '-Wl,$(AS_SYSTEM_PATH)/D0434/SG4/ARM/libdvframe.a' '-Wl,$(AS_SYSTEM_PATH)/D0434/SG4/ARM/libCAN_Lib.a' '-Wl,$(AS_SYSTEM_PATH)/D0434/SG4/ARM/libAsIecCon.a' '-Wl,$(AS_SYSTEM_PATH)/D0434/SG4/ARM/libastime.a' '-Wl,$(AS_SYSTEM_PATH)/D0434/SG4/ARM/libruntime.a' '-Wl,$(AS_SYSTEM_PATH)/D0434/SG4/ARM/liboperator.a' -specs=ARMspecs_brelf -secret '$(AS_PROJECT_PATH)_br.as.ccompiler.exe'

$(AS_CPU_PATH)/PPMComm/Main.c.o: \
	$(AS_PROJECT_PATH)/Logical/PPMComm/Main.c \
	$(AS_PROJECT_PATH)/Logical/Libraries/CAN_Lib/CAN_Lib.fun \
	$(AS_PROJECT_PATH)/Logical/Global.var \
	$(AS_PROJECT_PATH)/Logical/Global.typ \
	$(AS_PROJECT_PATH)/Logical/PPMComm/Variables.var \
	$(AS_PROJECT_PATH)/Logical/PPMComm/Types.typ
	@'$(AS_BIN_PATH)/BR.AS.CCompiler.exe' '$(AS_PROJECT_PATH)/Logical/PPMComm/Main.c' -o '$(AS_CPU_PATH)/PPMComm/Main.c.o'  -T SG4  -M ARM  -B D4.34 -G V4.1.2  -s 'PPMComm' -t '$(AS_TEMP_PATH)' -specs=ARMspecs_brelf -I '$(AS_PROJECT_PATH)/Logical/PPMComm' '$(AS_TEMP_PATH)/Includes/PPMComm' '$(AS_TEMP_PATH)/Includes' -trigraphs -fno-asm -D _DEFAULT_INCLUDES -D _SG4 -fPIC -O0 -ggdb -Wall -include '$(AS_CPU_PATH)/Libraries.h' -x c -P '$(AS_PROJECT_PATH)' -secret '$(AS_PROJECT_PATH)_br.as.ccompiler.exe'

$(AS_CPU_PATH)/PPMComm/_bur_pvdef.c.o: \
	$(AS_PROJECT_PATH)/Logical/Libraries/CAN_Lib/CAN_Lib.fun \
	$(AS_PROJECT_PATH)/Logical/Global.var \
	$(AS_PROJECT_PATH)/Logical/Global.typ \
	$(AS_PROJECT_PATH)/Logical/PPMComm/Variables.var \
	$(AS_PROJECT_PATH)/Logical/PPMComm/Types.typ
	@'$(AS_BIN_PATH)/BR.AS.CCompiler.exe' '$(AS_PATH)/AS/GnuInst/V4.1.2/arm-elf/include/bur/_bur_pvdef.c' -o '$(AS_CPU_PATH)/PPMComm/_bur_pvdef.c.o'  -T SG4  -M ARM  -B D4.34 -G V4.1.2  -s 'PPMComm' -t '$(AS_TEMP_PATH)' -specs=ARMspecs_brelf -I '$(AS_PROJECT_PATH)/Logical/PPMComm' '$(AS_TEMP_PATH)/Includes/PPMComm' '$(AS_TEMP_PATH)/Includes' -trigraphs -fno-asm -D _DEFAULT_INCLUDES -D _SG4 -fPIC -O0 -ggdb -Wall -include '$(AS_CPU_PATH)/Libraries.h' -P '$(AS_PROJECT_PATH)' 
 -secret '$(AS_PROJECT_PATH)_br.as.ccompiler.exe'

-include $(AS_CPU_PATH)/Force.mak 



FORCE: