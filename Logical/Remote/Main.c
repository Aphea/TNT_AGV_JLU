
#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

void _INIT ProgramInit(void)
{
	CANqueue_3.enable = 1;
	CANqueue_3.us_ident = CANopen_0.us_ident;
	CANqueue_3.can_id = 0x213;
	CANqueue_3.size = 8;
	CANqueue(&CANqueue_3);
	
	CANrd_3.enable = 1;
	CANrd_3.q_ident = CANqueue_3.q_ident;
	CANrd_3.data_adr = (UDINT)remote_ch_value;
}

void _CYCLIC ProgramCyclic(void)
{
	CANrd(&CANrd_3);
	
	if((remote_ch_value[0]!=0) && (remote_ch_value[1]!=0) && (remote_ch_value[2]!=0) && (remote_ch_value[3]!=0) &&
		(remote_ch_value[4]!=0) && (remote_ch_value[5]!=0) && (remote_ch_value[6]!=0) && (remote_ch_value[7]!=0))
	{
		DrivingModel.Remote.steer_ch_value = remote_ch_value[0];
		DrivingModel.Remote.throttle_ch_value = remote_ch_value[2];
		DrivingModel.Remote.direction_ch_value = remote_ch_value[7];
		DrivingModel.Remote.stop_ch_value = remote_ch_value[4];
		if((DrivingModel.Remote.throttle_ch_value < DrivingModel.Remote.min_value) 
			|| (DrivingModel.Remote.throttle_ch_value > DrivingModel.Remote.max_value))
		{
			if(int_count > 5)
			{
				DrivingModel.Remote.signal_OK = 0;
				over = 1;
			}
			else
			{
				int_count ++;
			}
		}
		else
		{
			DrivingModel.Remote.signal_OK = 1;
			int_count = 0;
		}
		
		if(DrivingModel.Remote.signal_OK == 1)
		{
			//确定油门
			DrivingModel.Remote.throttle = ((DrivingModel.Remote.max_value - DrivingModel.Remote.throttle_ch_value) * 100) / (DrivingModel.Remote.max_value - DrivingModel.Remote.min_value);
			//确定进退
			if(((DrivingModel.Remote.direction_ch_value-DrivingModel.Remote.min_value)<5) && ((DrivingModel.Remote.min_value-DrivingModel.Remote.direction_ch_value)<5))
			{
				DrivingModel.Remote.direction = 0;//前进
				DrivingModel.Remote.control = 1;
			}
			else if(((DrivingModel.Remote.direction_ch_value-DrivingModel.Remote.max_value)<5) && ((DrivingModel.Remote.max_value-DrivingModel.Remote.direction_ch_value)<5))
			{
				DrivingModel.Remote.direction = 1;//后退
				DrivingModel.Remote.control = 1;
			}
			else
			{
				DrivingModel.Remote.direction = 2;//P挡
				DrivingModel.Remote.control = 0;//遥控器取消介入
			}
			//确定速度
			if(DrivingModel.Remote.direction == 0)
			{
				Command.remote_speed_cmd = (INT)(((REAL)DrivingModel.Remote.throttle/100) * DrivingModel.Remote.throttle_max);
			}
			else if(DrivingModel.Remote.direction == 1)
			{
				Command.remote_speed_cmd = (INT)(((REAL)DrivingModel.Remote.throttle/100) * DrivingModel.Remote.throttle_max) * (-1);
			}
			else
			{
				Command.remote_speed_cmd = 0;
			}
			//确定转向
			DrivingModel.Remote.steer = (DrivingModel.Remote.steer_ch_value - DrivingModel.Remote.min_value) * 100 / (DrivingModel.Remote.max_value-DrivingModel.Remote.min_value);
			if((DrivingModel.Remote.steer <= 51) && (DrivingModel.Remote.steer >=49))
			{
				Command.remote_steer_cmd = 0;
			}
			else if(DrivingModel.Remote.steer < 50)
			{
				Command.remote_steer_cmd = (INT)(((REAL)(50-DrivingModel.Remote.steer)/50)*DrivingModel.Remote.steer_max);
			}
			else if(DrivingModel.Remote.steer > 50)
			{
				Command.remote_steer_cmd = ((INT)(((REAL)(DrivingModel.Remote.steer - 50)/50)*DrivingModel.Remote.steer_max)) * (-1);
			}
			//确定急停
			if(((DrivingModel.Remote.stop_ch_value-DrivingModel.Remote.min_value)<2) && ((DrivingModel.Remote.min_value-DrivingModel.Remote.stop_ch_value)<2))
			{
				DrivingModel.Remote.stop = 0;//正常
			}
			else
			{
				DrivingModel.Remote.stop = 1;//急停
			}
		}
	}
	else
	{
		DrivingModel.Remote.signal_OK = 0;
	}
}

void _EXIT ProgramExit(void)
{

}

