
#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

void _INIT ProgramInit(void)
{

}

void _CYCLIC ProgramCyclic(void)
{	
	/***********运行模式确定************/
	//急停
	if((DIMapping.QuickStop == 1) || 		//远程急停
		(DrivingModel.Remote.stop == 1) || 	//航模遥控器急停
		((DrivingModel.Can.park == 1) && (DrivingModel.Remote.control == 0)) || //CAN接口急停/驻车
		((DrivingModel.Serial.park == 1) && (DrivingModel.Remote.control == 0)))//串口急停/驻车
	{
		DrivingModel.RunMode = 3;//急停模式
		DOMapping.ModeLed1 = 1;
		DOMapping.ModeLed2 = 1;
	}
	else if(DIMapping.LockOff == 1)
	{
		DrivingModel.RunMode = 0;//空挡模式
		DOMapping.ModeLed1 = 0;
		DOMapping.ModeLed2 = 0;
	}
	else if(DIMapping.LockOff == 0)
	{
		if((DrivingModel.Serial.signal_OK==1) ||
			(DrivingModel.Can.signal_OK==1) || 
			(DrivingModel.Remote.signal_OK == 1))
		{
			DrivingModel.RunMode = 1;//正常运行模式
			DOMapping.ModeLed1 = 0;
			DOMapping.ModeLed2 = 1;
		}
		else
		{
			DrivingModel.RunMode = 2;//通讯中断模式
			DOMapping.ModeLed1 = 1;
			DOMapping.ModeLed2 = 0;
		}
	}
	
	
	//回正完毕，开始运行应用层主循环

	/***********相应模式的参数设置************/
	/*************正常运行模式****************/
	if((DrivingModel.RunMode == 1) && (DrivingModel.ESP.homing_OK == 1)) 
	{	
		if(DrivingModel.Remote.control == 1)//航模遥控器控制
		{
			DrivingModel.MCU.speed_cmd = Command.remote_speed_cmd;
			DrivingModel.ESP.angle_cmd = Command.remote_steer_cmd;
		}
		else 								//串口控制
		{
			if(DrivingModel.Serial.signal_OK == 1)
			{
				DrivingModel.MCU.speed_cmd = Command.pc_speed_cmd;
				DrivingModel.ESP.angle_cmd = Command.pc_steer_cmd;
			}
			else if(DrivingModel.Can.signal_OK == 1)
			{
				DrivingModel.MCU.speed_cmd = Command.can_speed_cmd;
				DrivingModel.ESP.angle_cmd = Command.can_steer_cmd;
			}
			else
			{
				DrivingModel.MCU.speed_cmd = 0;
				DrivingModel.ESP.angle_cmd = 0;
			}
			
		}
		DOMapping.LockOff = 1;//解抱闸
	}	
	/*************通讯中断模式****************/
	else if(DrivingModel.RunMode == 2)
	{
		DrivingModel.MCU.speed_cmd = 0;

		DOMapping.LockOff = 0;//抱闸
		DrivingModel.Brake.start = 1;
	}
	/*************急停模式****************/
	else if(DrivingModel.RunMode == 3)
	{
		DrivingModel.MCU.speed_cmd = 0;

		DOMapping.LockOff = 0;//抱闸
		DrivingModel.Brake.start = 1;
	}
	/*************空挡模式****************/			
	else if(DrivingModel.RunMode == 0)
	{
		DrivingModel.MCU.speed_cmd = 0;

		DOMapping.LockOff = 1;//解抱闸
		DrivingModel.Brake.start = 0;
	}
}

void _EXIT ProgramExit(void)
{

}
