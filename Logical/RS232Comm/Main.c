
#include <bur/plctypes.h>
#include <string.h>
#ifdef _DEFAULT_INCLUDES
#include <AsDefault.h>
#endif

void SendData(UDINT data_adr);
void ReceiveData(UDINT data_adr);

void _INIT ProgramInit(void)
{
	FRM_xopen_0.enable 	= 1;
	FRM_xopen_0.device 	= (UDINT)"IF1";
	FRM_xopen_0.mode 	= (UDINT)"/BD=115200 /DB=8 /PA=N /SB=1";
	FRM_xopen_0.config 	= (UDINT)&XOPENCONFIG_0;
	FRM_xopen(&FRM_xopen_0);
	
}

/*
	串口接收标志位说明
	rx_rec_OK:串口报文接收成功
	rx_headtail_OK:报文首尾字符正确
	rx_sum_OK:校验正确
	rx_repeat_OK:报文未重复
	rx_rec_cmd_OK:报文正确
*/
void _CYCLIC ProgramCyclic(void)
{
	USINT i, check_sum = 0;;
	ReceiveData((UDINT)rx_data);
	if(rx_rec_OK == 1)
	{
		RecTimeLast = clock_ms();
		/**接收报文是否正确判断**/
		if((rx_data[0] == 0xAA) && (rx_data[15] == 0x55))
		{
			rx_headtail_OK = 1;
			
			/**校验位判断**/
			for(i=0; i<14; i++)
			{
				check_sum += rx_data[i];
			}
			if(check_sum == rx_data[14])
			{
				rx_sum_OK = 1;
				
				/**接收报文是否重复判断**/
				if(rx_data[1] != ReaptLast)
				{
					rx_repeat_OK = 1;
					ReaptLast = rx_data[1];
					rx_rec_cmd_OK = 1;
					DrivingModel.Serial.interrupt_count = 0;
				}
				else
				{
					rx_repeat_OK = 0;
					rx_rec_cmd_OK = 0;
				}
			}
			else
			{
				rx_sum_OK = 0;
				rx_rec_cmd_OK = 0;
			}
		}
		else
		{
			rx_headtail_OK = 0;
			rx_rec_cmd_OK = 0;
		}
			
		/**控制模式功能指令**/
		if(rx_rec_cmd_OK == 1)
		{
			DrivingModel.Serial.CommFirst = 1;
			DrivingModel.Serial.signal_OK = 1;
			
			/**模式4(速度模式) 线速度、角速度给定**/
			if(rx_data[2] == 0x00)//驻车
			{
				DrivingModel.Serial.park = 1;
				Command.pc_speed_cmd = 0;//线速度给定
				Command.pc_steer_cmd = 0;//角度给定
			}
			if(rx_data[2] == 0x01)//行车
			{
				DrivingModel.Serial.park = 0;
				Command.pc_speed_cmd = (INT)((UINT)rx_data[4]*256 + rx_data[5]);//线速度给定
				Command.pc_steer_cmd = (INT)((UINT)rx_data[8]*256 + rx_data[9]);//角度给定
			}
		}
		else
		{
			DrivingModel.Serial.signal_OK = 0;
		}
	}
	else//超过200ms没有接收到命令
	{
		if(DrivingModel.Serial.interrupt_count > 20)
		{
			DrivingModel.Serial.signal_OK = 0;
		}
		else
		{
			DrivingModel.Serial.interrupt_count ++;
		}
		
		#if 0
		RecIntervals = clock_ms() - RecTimeLast;
		if(RecIntervals > OverTime)
		{
			//DrivingModel.Serial.CommOver = 1;
			DrivingModel.Serial.signal_OK = 0;
			g_bRecOverTime = 1;
		}
		else
		{
			DrivingModel.Serial.CommOver = 0;
			g_bRecOverTime = 0;
		}	
		#endif
	}
	
	tx_data[0] = 0xAA;//帧开始
	tx_data[2] = 0x01;

	tx_data[4] = (UINT)DrivingModel.MCU.speed_act / 256;
	tx_data[5] = (UINT)DrivingModel.MCU.speed_act % 256;
	tx_data[8] = (UINT)DrivingModel.ESP.angle_act / 256;
	tx_data[9] = (UINT)DrivingModel.ESP.angle_act % 256;

	tx_data[11] = Battery.Percentage;
	
	tx_data[23] = 0x55;//帧结束

	if(rx_rec_cmd_OK == 1)
	{
		rx_rec_cmd_OK = 0;
		SendData((UDINT)tx_data);//发送
	}
}

void _EXIT ProgramExit(void)
{

}

void ReceiveData(UDINT data_adr)
{
	UDINT Read_Data;
	FRM_read_0.enable = 1;
	FRM_read_0.ident = FRM_xopen_0.ident;
	FRM_read(&FRM_read_0);
	Read_Data = (UDINT)FRM_read_0.buffer;
	if(FRM_read_0.buflng != 0)
	{
		memcpy((UDINT *)data_adr, (UDINT *)Read_Data, 16*sizeof(USINT));
		FRM_rbuf_0.enable = 1;
		FRM_rbuf_0.ident = FRM_xopen_0.ident;
		FRM_rbuf_0.buffer = Read_Data;
		FRM_rbuf_0.buflng = 16;
		FRM_rbuf(&FRM_rbuf_0);
		rx_rec_OK = 1;
	}
	else
	{
		rx_rec_OK = 0;
	}
}

void SendData(UDINT data_adr)
{
	UDINT Write_Data;
	FRM_gbuf_0.enable = 1;
	FRM_gbuf_0.ident = FRM_xopen_0.ident;
	FRM_gbuf(&FRM_gbuf_0);
	Write_Data = (UDINT)FRM_gbuf_0.buffer;
	memcpy((UDINT *)Write_Data, (UDINT *)data_adr, 24*sizeof(USINT));
	FRM_write_0.enable = 1;
	FRM_write_0.ident = FRM_xopen_0.ident;
	FRM_write_0.buffer = FRM_gbuf_0.buffer;
	FRM_write_0.buflng = 24;
	FRM_write(&FRM_write_0);
}
