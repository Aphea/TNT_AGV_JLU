
#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

void _INIT ProgramInit(void)
{
	if(CANopen_0.status == 0)
	{
		CANquwr_1.enable = 1;
		CANquwr_1.us_ident = CANopen_0.us_ident;
		CANquwr_2.enable = 1;
		CANquwr_2.us_ident = CANopen_0.us_ident;
		
		CANqueue_2.enable = 1;
		CANqueue_2.us_ident = CANopen_0.us_ident;
		CANqueue_2.can_id = 0x20;
		CANqueue_2.size = 8;
		CANqueue(&CANqueue_2);
		CANqueue_3.enable = 1;
		CANqueue_3.us_ident = CANopen_0.us_ident;
		CANqueue_3.can_id = 0x30;
		CANqueue_3.size = 8;
		CANqueue(&CANqueue_3);
	}
}

void _CYCLIC ProgramCyclic(void)
{
	int i;
	
	if(CAN_cycle_count == 0)
	{
		CAN_cycle_count = 1;
		CAN_info_0[0] = (UINT)DrivingModel.MCU.speed_act % 256;
		CAN_info_0[1] = (UINT)DrivingModel.MCU.speed_act / 256;
		CAN_info_0[2] = (UINT)DrivingModel.ESP.angle_act % 256;
		CAN_info_0[3] = (UINT)DrivingModel.ESP.angle_act / 256;
		CAN_info_0[4] = 0;
		CAN_info_0[5] = 0;
		CAN_info_0[6] = CAN_info_0_count ++;
		CAN_info_0[7] = 0;
		for(i=0; i<7; i++)
		{
			CAN_info_0[7] += CAN_info_0[i];
		}
			
		CANquwr_1.can_id = 0x40;
		CANquwr_1.data_adr = CAN_info_0;
		CANquwr_1.data_lng = 8;
		CANquwr(&CANquwr_1);
	
		
		CANrd_2.enable = 1;
		CANrd_2.q_ident = CANqueue_2.q_ident;
		CANrd_2.data_adr = CAN_cmd_0;
		CANrd(&CANrd_2);

		if(CANrd_2.status == 0)
		{
			CAN_cmd_0_sum = 0;
			for(i=0; i<7; i++)
			{
				CAN_cmd_0_sum += CAN_cmd_0[i];
			}
			if(CAN_cmd_0_sum == CAN_cmd_0[7])//校验成功
			{
				if(Last_id != CAN_cmd_0[6])
				{
					DrivingModel.Can.signal_OK = 1;
					DrivingModel.Can.interrupt_count = 0;
					Last_id = CAN_cmd_0[6];
					if(CAN_cmd_0[0] == 0x00)//驻车模式
					{
						DrivingModel.Can.park = 1;
					}
					else if(CAN_cmd_0[0] == 0x01)//行进模式
					{
						DrivingModel.Can.park = 0;
						Command.can_speed_cmd = (INT)((UINT)CAN_cmd_0[2]*256 + CAN_cmd_0[1]);
						Command.can_steer_cmd = (INT)((UINT)CAN_cmd_0[4]*256 + CAN_cmd_0[3]);
					}
				}
				else
				{
					DrivingModel.Can.signal_OK = 0;
				}
			}
			else
			{
				DrivingModel.Can.signal_OK = 0;
			}
		}
		else
		{
			if(DrivingModel.Can.interrupt_count > 20)
			{
				DrivingModel.Can.signal_OK = 0;
			}
			else
			{
				DrivingModel.Can.interrupt_count ++;
			}
		}
	}
	else
	{
		CAN_cycle_count = 0;
		CAN_info_1[0] = Battery.Percentage;
		CAN_info_1[1] = MileMeter;
		CAN_info_1[2] = DrivingModel.Temprature / 10;
		
		CAN_info_1[6] = CAN_info_1_count ++;
		CAN_info_1[7] = 0;
		for(i=0; i<7; i++)
		{
			CAN_info_1[7] += CAN_info_1[i];
		}
			
		CANquwr_2.can_id = 0x50;
		CANquwr_2.data_adr = CAN_info_1;
		CANquwr_2.data_lng = 8;
		CANquwr(&CANquwr_2);
	}
}

void _EXIT ProgramExit(void)
{

}

