
TYPE
	ProPosMode_type : 	STRUCT 
		wRel : BOOL;
		wAbs : BOOL;
		wTargetPos : DINT;
		wTargetPosSend : DINT;
		wProVlocSend : UDINT;
		wProVloc : UDINT;
	END_STRUCT;
	ProVelMode_type : 	STRUCT 
		wSetSpd : DINT;
		wSetSpdSend : DINT;
	END_STRUCT;
	ElmoMotor_type : 	STRUCT 
		wCINH : BOOL;
		wBG : BOOL;
		wQSP : BOOL;
		wControlWord : UINT;
		wDrvMode : SINT;
		wOpMode1 : ProPosMode_type;
		wOpMode3 : ProVelMode_type;
		rCINH : UINT;
		rActSpeed : DINT;
		rActCurrent : INT;
		rDrvMode : USINT;
		rEncCount : DINT;
		errCode : UINT;
		rCommOK : BOOL := FALSE;
	END_STRUCT;
	Battery_typ : 	STRUCT 
		Voltage : UINT;
		Current : INT; (*Charging +     Discharging -*)
		RestCapacity : UINT;
		NominalCapacity : UINT := 0;
		CyclicCount : INT := 0;
		Percentage : UINT := 0;
		ProtectStatus : UINT;
	END_STRUCT;
	VCMC_typ : 	STRUCT  (*VCU control MCU command	*)
		VCU_Shiftlevel_Req : USINT;
		VCU_Control_Enable : USINT;
		VCU_RPM_Req : UINT;
		VCU_Torque_Req : UINT;
		VCU_Brake_feedback_Req : USINT;
		MCU_CheckSum : USINT;
	END_STRUCT;
	MCUStatus1_typ : 	STRUCT 
		MCU_Current_Status : USINT;
		MCU_Energy_Feedback : BOOL;
		Drive_Mode : BOOL;
		MCU_Current_Gear : USINT;
		MCU_Current_Torque : UINT;
		MCU_Current_RPM : UINT;
		MCU_Current_Temperature : USINT;
		MCU_Diag : USINT;
		MCU_CheckSum : USINT;
	END_STRUCT;
	MCUStatus2_typ : 	STRUCT 
		MCU_Voltage : UINT;
		MCU_Current : UINT;
		MCU_CheckSum : USINT;
	END_STRUCT;
	DrivingModel_typ : 	STRUCT 
		RunMode : USINT := 0;
		remote_stop : BOOL := FALSE;
		Temprature : INT;
		ESP : ESP_typ;
		MCU : MCU_typ;
		Serial : Serial_typ;
		Gyro : Gyro_typ;
		Brake : Brake_typ;
		Remote : Remote_typ;
		Can : Can_typ;
	END_STRUCT;
	DOMapping_typ : 	STRUCT 
		PowerLed1 : BOOL;
		PowerLed2 : BOOL;
		PowerLed3 : BOOL;
		PowerLed4 : BOOL;
		StatusLed : BOOL;
		LockOff : BOOL;
		BrakeEnable : BOOL;
		BrakeStart : BOOL;
		ModeLed0 : BOOL;
		ModeLed1 : BOOL;
		ModeLed2 : BOOL;
	END_STRUCT;
	DIMapping_typ : 	STRUCT 
		QuickStop : BOOL;
		LockOff : BOOL;
		HomingInput : BOOL;
		RemoteStop : BOOL;
	END_STRUCT;
	ConstModel_typ : 	STRUCT 
		ProductModel : STRING[11] := 'TNT_AGV_007';
		ProgramVersion : STRING[20] := 'TNT_AGV_JLU_V10';
		ProduceDate : STRING[10] := '2018/12/05';
		BackWheelDiameter : REAL := 400;
		Tread : REAL := 800; (*The distance between the left wheel and the right wheel*)
		WheelBase : REAL := 1000; (*The distance between the front wheel and the back wheel*)
		TurningZeroPoint : DINT := 16135;
		TurningSpeed : DINT := 150000;
		LinearRatio : REAL := 12;
		LineSpeedLimit : UINT := 10000;
		SteerAngleRatio : UINT := 10488;
		CorrectK : REAL := 3.7;
		SteerCurrentLimit : INT := 1000;
		VCUTempratureLimit : INT;
	END_STRUCT;
	SysFault_typ : 	STRUCT 
		FaultProduce : BOOL;
		MCU_Fault : UINT;
		BMS_Fault : UINT;
		EPS_Fault : UINT;
		VCU_Fault : UINT;
		FaultCode : ARRAY[0..7]OF USINT;
		FaultNum : USINT;
	END_STRUCT;
	VCUStatus_typ : 	STRUCT 
		Temprature : INT;
		Module2Status : BOOL;
		Module3Status : BOOL;
		Module4Status : BOOL;
		Module5Status : BOOL;
		Module6Status : BOOL;
	END_STRUCT;
	PIDControl_typ : 	STRUCT 
		SetPoint : INT;
		Proportion : DINT := 0;
		Integral : REAL := 0.0;
		Derivative : DINT := 0;
		LastError : INT := 0;
		PrevError : INT := 0;
		SumError : INT := 0;
		Error : INT;
		dError : INT;
		ret : DINT;
		NowPoint : INT;
	END_STRUCT;
	ESP_typ : 	STRUCT 
		homing_OK : BOOL := 0;
		homing_point : DINT;
		angle_cmd : DINT;
		angle_act : DINT;
		angle_limit : INT := 230;
	END_STRUCT;
	MCU_typ : 	STRUCT 
		shift_level_req : USINT := 0; (*0->P;1->R;2->N;3->D;4->S;*)
		shift_level_act : USINT;
		speed_cmd : INT;
		speed_cmd_correct : INT;
		speed_req : INT;
		speed_act : INT;
		speed_limit : INT := 300;
	END_STRUCT;
	Brake_typ : 	STRUCT 
		start : BOOL;
		count : USINT;
	END_STRUCT;
	Serial_typ : 	STRUCT 
		CommOver : USINT := 1;
		CommFirst : USINT := 0;
		signal_OK : BOOL := FALSE;
		park : BOOL := 0;
		control : BOOL := 0;
		interrupt_count : USINT;
	END_STRUCT;
	Storage_typ : 	STRUCT 
		homing_distance : DINT := 140000;
		SteerCorrectLeft : INT;
		SteerCorrectRight : INT;
		SteerAngleLimit : REAL := 22.7;
		TurningSpeed : DINT := 150000;
		LineSpeedLimit : UINT := 10000;
	END_STRUCT;
	Gyro_typ : 	STRUCT 
		Angle : INT;
		AngleSpeed : INT;
		SignalOK : BOOL;
	END_STRUCT;
	Command_typ : 	STRUCT 
		remote_speed_cmd : INT;
		remote_steer_cmd : INT;
		pc_speed_cmd : INT;
		pc_steer_cmd : INT;
		can_speed_cmd : INT;
		can_steer_cmd : INT;
	END_STRUCT;
	Remote_typ : 	STRUCT 
		signal_OK : BOOL;
		steer_ch_value : USINT;
		throttle_ch_value : USINT;
		direction_ch_value : USINT;
		stop_ch_value : USINT;
		steer : USINT;
		throttle : USINT;
		direction : USINT;
		max_value : USINT := 129;
		min_value : USINT := 73;
		throttle_max : UINT := 300;
		steer_max : UINT := 230;
		stop : BOOL := 0;
		control : BOOL := 0;
	END_STRUCT;
	Can_typ : 	STRUCT 
		signal_OK : BOOL;
		park : BOOL := 0;
		interrupt_count : USINT := 0;
	END_STRUCT;
END_TYPE
