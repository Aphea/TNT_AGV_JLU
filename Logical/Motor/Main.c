
#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

void _INIT ProgramInit(void)
{
	CANopen_0.enable = 1;
	CANopen_0.baud_rate = 50;
	CANopen_0.cob_anz = 5;
	CANopen_0.device = (UDINT)"IF7";
	CANopen(&CANopen_0);
	
	if(CANopen_0.status == 0)
	{
		CANqueue_0.enable = 1;
		CANqueue_0.us_ident = CANopen_0.us_ident;
		CANqueue_0.can_id = 0x160;
		CANqueue_0.size = 8;
		CANqueue(&CANqueue_0);
	
		CANqueue_1.enable = 1;
		CANqueue_1.us_ident = CANopen_0.us_ident;
		CANqueue_1.can_id = 0x170;
		CANqueue_1.size = 8;
		CANqueue(&CANqueue_1);
	
		CANquwr_0.enable = 1;
		CANquwr_0.us_ident = CANopen_0.us_ident;
	}
}

void _CYCLIC ProgramCyclic(void)
{
	if(DrivingModel.MCU.speed_cmd > DrivingModel.MCU.speed_limit)
	{
		DrivingModel.MCU.speed_cmd = DrivingModel.MCU.speed_limit;
	}
	else if(DrivingModel.MCU.speed_cmd < (-1)*DrivingModel.MCU.speed_limit)
	{
		DrivingModel.MCU.speed_cmd = (-1)*DrivingModel.MCU.speed_limit;
	}
		
	DrivingModel.MCU.speed_cmd_correct = (INT)((REAL)DrivingModel.MCU.speed_cmd / ConstModel.CorrectK);
	//Linespeed_cmd和Linespeed_act的单位都是0.01m/s		Linespeed_req单位是1RPM
	if(DrivingModel.MCU.speed_cmd > 0)
	{
		DrivingModel.Brake.start = 0;
		DrivingModel.MCU.shift_level_req = 3;//D 挡
			
		//RPM=(下发速度/轮胎周长)*60*差速比*10		乘10是因为单位是0.1RPM
		DrivingModel.MCU.speed_req = (REAL)(DrivingModel.MCU.speed_cmd_correct/(100*3.14*(ConstModel.BackWheelDiameter/1000))) * 60 * 10 * ConstModel.LinearRatio;
	}
	else if(DrivingModel.MCU.speed_cmd < 0)
	{
		DrivingModel.Brake.start = 0;
		DrivingModel.MCU.shift_level_req = 1;//R 挡		
		//RPM=(下发角度/轮胎周长)*60*差速比*10		乘10是因为单位是0.1RPM
		DrivingModel.MCU.speed_req = (REAL)((DrivingModel.MCU.speed_cmd_correct*(-1))/(100*3.14*(ConstModel.BackWheelDiameter/1000))) * 60 * 10 * ConstModel.LinearRatio;
	}
	else
	{
		DrivingModel.MCU.shift_level_req = 0;//P 挡
		DrivingModel.MCU.speed_req = 0;
		
		DrivingModel.Brake.start = 1;
	}
	
	
	VCMC.VCU_Shiftlevel_Req = DrivingModel.MCU.shift_level_req;
	VCMC.VCU_Control_Enable = 1;
	VCMC.VCU_RPM_Req = DrivingModel.MCU.speed_req;
	
	TranFrame[0] = (USINT)((VCMC.VCU_Shiftlevel_Req&0x0F) + ((VCMC.VCU_Control_Enable&0x03)<<4));
	TranFrame[1] = (USINT)(VCMC.VCU_RPM_Req&0x00FF);
	TranFrame[2] = (USINT)((VCMC.VCU_RPM_Req&0xFF00)>>8);
	TranFrame[3] = (USINT)(VCMC.VCU_Torque_Req&0x00FF);
	TranFrame[4] = (USINT)((VCMC.VCU_Torque_Req&0xFF00)>>8);
	TranFrame[5] = (USINT)(VCMC.VCU_Brake_feedback_Req);
	TranFrame[6] = 0;
	VCMC.MCU_CheckSum = TranFrame[0]^TranFrame[1]^TranFrame[2]^TranFrame[3]^TranFrame[4]^TranFrame[5]^TranFrame[6];
	TranFrame[7] = (USINT)VCMC.MCU_CheckSum;
	
	CANquwr_0.can_id = 0x140;
	CANquwr_0.data_adr = (UDINT)TranFrame;
	CANquwr_0.data_lng = 8;
	CANquwr(&CANquwr_0);
	
	if(switchFlag == 0)
	{
		switchFlag = 1;
		CANrd_0.enable = 1;
		CANrd_0.q_ident = CANqueue_0.q_ident;
		CANrd_0.data_adr = (UDINT)RecFrame1;
		CANrd(&CANrd_0);
	
		if(CANrd_0.status == 0)
		{
			if(RecFrame1[7] == RecFrame1[0]^RecFrame1[1]^RecFrame1[2]^RecFrame1[3]^RecFrame1[4]^RecFrame1[5]^RecFrame1[6])
			{
				MCUStatus1.MCU_Current_Status 		= RecFrame1[0]&0x03;
				MCUStatus1.MCU_Energy_Feedback 		= (RecFrame1[0]&0x04)>>2;
				MCUStatus1.Drive_Mode 				= (RecFrame1[0]&0x08)>>3;
				MCUStatus1.MCU_Current_Gear			= (RecFrame1[0]&0xF0)>>4;
				MCUStatus1.MCU_Current_Torque   	= ((UINT)RecFrame1[2])*256 + RecFrame1[1];
				MCUStatus1.MCU_Current_RPM			= ((UINT)RecFrame1[4])*256 + RecFrame1[3];
				MCUStatus1.MCU_Current_Temperature 	= RecFrame1[5];
				MCUStatus1.MCU_Diag					= RecFrame1[6];
				MCUStatus1.MCU_CheckSum				= RecFrame1[7];
			}
		}
	}
	else
	{
		switchFlag = 0;
		CANrd_1.enable = 1;
		CANrd_1.q_ident = CANqueue_1.q_ident;
		CANrd_1.data_adr = (UDINT)RecFrame2;
		CANrd(&CANrd_1);
		if(CANrd_1.status == 0)
		{
			if(RecFrame2[7] == RecFrame2[0]^RecFrame2[1]^RecFrame2[2]^RecFrame2[3]^RecFrame2[4]^RecFrame2[5]^RecFrame2[6])
			{
				MCUStatus2.MCU_Voltage 	= ((UINT)RecFrame2[1])*256 + RecFrame2[0];
				MCUStatus2.MCU_Current 	= ((UINT)RecFrame2[3])*256 + RecFrame2[2];
				MCUStatus2.MCU_CheckSum = RecFrame2[7];
			}
		}
	}
	
	DrivingModel.MCU.shift_level_act = MCUStatus1.MCU_Current_Gear;	
	DrivingModel.MCU.speed_act = ((INT)((((REAL)MCUStatus1.MCU_Current_RPM/(60*10*ConstModel.LinearRatio)) * (3.14*(ConstModel.BackWheelDiameter/1000))) * 100)) * ConstModel.CorrectK;
	if(DrivingModel.MCU.shift_level_act == 2)
	{
		DrivingModel.MCU.speed_act = DrivingModel.MCU.speed_act * (-1);
	}
	
	//里程计实现
	//		 10ms									  速度系数
	if(DrivingModel.MCU.speed_act >= 0)
	{
		Meter += 0.004 * (REAL)DrivingModel.MCU.speed_act * 0.01;
	}
	else
	{
		Meter += 0.004 * (REAL)DrivingModel.MCU.speed_act * 0.01 * (-1);
	}
	if(Meter > 1000)//大于1公里，公里数+1
	{
		MileMeter ++;
		Meter -= 1000;
	}
}

void _EXIT ProgramExit(void)
{

}

