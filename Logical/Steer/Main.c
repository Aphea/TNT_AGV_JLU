
#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

UDINT InqPx(UDINT can_id);
UDINT InqJv(UDINT can_id);
UDINT InqVx(UDINT can_id);
UDINT InqIq(UDINT can_id);
UDINT InqAc(UDINT can_id);
UDINT InqSp(UDINT can_id);
UDINT InqMo(UDINT can_id);
UDINT InqUm(UDINT can_id);
UDINT SetMo(UDINT can_id, BOOL value);
UDINT SetUm(UDINT can_id, USINT value);
UDINT SetJv(UDINT can_id, UDINT value);
UDINT SetPa(UDINT can_id, DINT value);
UDINT SetAc(UDINT can_id, DINT value);
UDINT SetSp(UDINT can_id, DINT value);
UDINT SetBg(UDINT can_id);
UDINT SetSt(UDINT can_id);

void _INIT ProgramInit(void)
{
	CANquwr_1.enable = 1;
	CANquwr_1.us_ident = CANopen_0.us_ident;
	
	tran_frame[1] = 0x00;
	tran_frame[0] = 0x01;
	CANquwr_1.can_id = 0x000;
	CANquwr_1.data_adr = tran_frame;
	CANquwr_1.data_lng = 2;
	CANquwr(&CANquwr_1);
	
	CANqueue_2.enable = 1;
	CANqueue_2.us_ident = CANopen_0.us_ident;
	CANqueue_2.can_id = Elmo_ID - 0x80;
	CANqueue_2.size = 8;
	CANqueue(&CANqueue_2);
	
	CANrd_2.enable = 1;
	CANrd_2.q_ident = CANqueue_2.q_ident;
	CANrd_2.data_adr = (UDINT)RecFrame;
}

void _CYCLIC ProgramCyclic(void)
{
	if(set_demand == 0)//查询
	{
		switch(inq_step)
		{
			case 0: InqMo(Elmo_ID);		break;
			case 1: InqUm(Elmo_ID);		break;
			case 2:	InqPx(Elmo_ID);		break;
			case 3: InqJv(Elmo_ID);		break;
			case 4:	InqVx(Elmo_ID);		break;
			case 5: InqIq(Elmo_ID);		break;
			case 6:	InqAc(Elmo_ID);		break;
			case 7:	InqSp(Elmo_ID);		break;
			default:					break;
		}
		inq_step ++;
		if(inq_step > 7)
		{
			inq_step = 0;
		}
		set_demand = 1;
	}
	else//设置
	{
		if(DrivingModel.ESP.homing_OK == 0)//寻找home点
		{
			if(DIMapping.LockOff == 0)//上电前空挡按钮未按下
			{
				switch(set_step)
				{
					case 0: SetMo(Elmo_ID, 0);			break;
					case 1: SetUm(Elmo_ID, 2);			break;
					case 2: SetJv(Elmo_ID, 30000);		break;
					case 3: SetMo(Elmo_ID, 1);			break;
					case 4: SetBg(Elmo_ID);				break;
					default:							break;
				}
				if(set_step < 4)
				{
					set_step ++;
				}
				if(DIMapping.HomingInput == 0)
				{
					SetSt(Elmo_ID);
					DrivingModel.ESP.homing_OK = 1;
					DrivingModel.ESP.homing_point = Elmo.PX;
					set_step = 0;
					run_status = 0;
				}
			}
		}
		else//寻找home点成功
		{
			if(DrivingModel.ESP.angle_cmd > DrivingModel.ESP.angle_limit)
			{
				DrivingModel.ESP.angle_cmd = DrivingModel.ESP.angle_limit;
			}
			else if(DrivingModel.ESP.angle_cmd < (DrivingModel.ESP.angle_limit * (-1)))
			{
				DrivingModel.ESP.angle_cmd = DrivingModel.ESP.angle_limit * (-1);
			}
				
			target_pos = DrivingModel.ESP.homing_point - Storage.homing_distance + DrivingModel.ESP.angle_cmd * 950;
			if(run_status == 0)
			{
				switch(set_step)
				{
					case 0:	SetMo(Elmo_ID, 0);			break;
					case 1:	SetSp(Elmo_ID, 0x50000);	break;//转速
					case 2: SetUm(Elmo_ID, 5);			break;
					case 3: SetMo(Elmo_ID, 1);			break;
					case 4:	SetJv(Elmo_ID, 1000);		break;
					case 5: SetAc(Elmo_ID, 800000);		break;
					case 6: SetPa(Elmo_ID, target_pos);	break;
					case 7: SetBg(Elmo_ID);				
							run_status = 1;				break;
					default:							break;
				}
				set_step ++;
				if(run_status == 1)
				{
					set_step = 0;
				}
			}
			else if(run_status == 1)
			{
				switch(set_step)
				{
					case 0:	SetPa(Elmo_ID, target_pos);	break;
					case 1: SetBg(Elmo_ID);				break;
					default:							break;
				}
				set_step ++;
				if(set_step == 2)
				{
					set_step = 0;
				}
			}
		}
		set_demand = 0;
	}
	
	CANrd(&CANrd_2);
	if(CANrd_2.status == 0)
	{
		if(CANrd_2.size == 8)
		{
			if((RecFrame[0] == 0x50) && (RecFrame[1] == 0x58))
			{
				Elmo.PX = RecFrame[7] * 0x100 * 0x100 * 0x100 + RecFrame[6] * 0x100 * 0x100 + RecFrame[5] * 0x100 + RecFrame[4];
			}
			else if((RecFrame[0] == 0x4D) && (RecFrame[1] == 0x4F))
			{
				Elmo.MO = RecFrame[4];
			}
			else if((RecFrame[0] == 0x55) && (RecFrame[1] == 0x4D))
			{
				Elmo.UM = RecFrame[4];
			}
			else if((RecFrame[0] == 0x50) && (RecFrame[1] == 0x41))
			{
				Elmo.PA = RecFrame[7] * 0x100 * 0x100 * 0x100 + RecFrame[6] * 0x100 * 0x100 + RecFrame[5] * 0x100 + RecFrame[4];
			}
			else if((RecFrame[0] == 0x56) && (RecFrame[1] == 0x58))
			{
				Elmo.VX = RecFrame[7] * 0x100 * 0x100 * 0x100 + RecFrame[6] * 0x100 * 0x100 + RecFrame[5] * 0x100 + RecFrame[4];			
			}
			else if((RecFrame[0] == 0x4A) && (RecFrame[1] == 0x56))
			{
				Elmo.JV = RecFrame[7] * 0x100 * 0x100 * 0x100 + RecFrame[6] * 0x100 * 0x100 + RecFrame[5] * 0x100 + RecFrame[4];			
			}
			else if((RecFrame[0] == 0x49) && (RecFrame[1] == 0x51))
			{
				Elmo.IQ = RecFrame[7] * 0x100 * 0x100 * 0x100 + RecFrame[6] * 0x100 * 0x100 + RecFrame[5] * 0x100 + RecFrame[4];
			}
			else if((RecFrame[0] == 0x41) && (RecFrame[1] == 0x43))
			{
				Elmo.AC = RecFrame[7] * 0x100 * 0x100 * 0x100 + RecFrame[6] * 0x100 * 0x100 + RecFrame[5] * 0x100 + RecFrame[4];
			}
			else if((RecFrame[0] == 0x53) && (RecFrame[1] == 0x50))
			{
				Elmo.SP = RecFrame[7] * 0x100 * 0x100 * 0x100 + RecFrame[6] * 0x100 * 0x100 + RecFrame[5] * 0x100 + RecFrame[4];
			}
		}	
	}
	//DrivingModel.ESP.angle_act = (int)((float)(Elmo.PX + Storage.homing_distance - DrivingModel.ESP.homing_point)/ConstModel.SteerAngleRatio*10);
	DrivingModel.ESP.angle_act = (int)((float)(Elmo.PX + Storage.homing_distance - DrivingModel.ESP.homing_point)/950);

	
}

void _EXIT ProgramExit(void)
{

}



UDINT InqPx(UDINT can_id)
{
	tran_frame[3] = 0x00;
	tran_frame[2] = 0x00;
	tran_frame[1] = 0x58;
	tran_frame[0] = 0x50;
	CANquwr_1.can_id = can_id;
	CANquwr_1.data_adr = tran_frame;
	CANquwr_1.data_lng = 4;
	CANquwr(&CANquwr_1);
	
	return CANquwr_1.status;
}
UDINT InqJv(UDINT can_id)
{
	tran_frame[3] = 0x00;
	tran_frame[2] = 0x00;
	tran_frame[1] = 0x56;
	tran_frame[0] = 0x4A;
	CANquwr_1.can_id = can_id;
	CANquwr_1.data_adr = tran_frame;
	CANquwr_1.data_lng = 4;
	CANquwr(&CANquwr_1);
	
	return CANquwr_1.status;
}
UDINT InqVx(UDINT can_id)
{
	tran_frame[3] = 0x00;
	tran_frame[2] = 0x00;
	tran_frame[1] = 0x58;
	tran_frame[0] = 0x56;
	CANquwr_1.can_id = can_id;
	CANquwr_1.data_adr = tran_frame;
	CANquwr_1.data_lng = 4;
	CANquwr(&CANquwr_1);
	
	return CANquwr_1.status;
}
UDINT InqIq(UDINT can_id)
{
	tran_frame[3] = 0x00;
	tran_frame[2] = 0x01;
	tran_frame[1] = 0x51;
	tran_frame[0] = 0x49;
	CANquwr_1.can_id = can_id;
	CANquwr_1.data_adr = tran_frame;
	CANquwr_1.data_lng = 4;
	CANquwr(&CANquwr_1);
	
	return CANquwr_1.status;
}
UDINT InqAc(UDINT can_id)
{
	tran_frame[3] = 0x00;
	tran_frame[2] = 0x00;
	tran_frame[1] = 0x43;
	tran_frame[0] = 0x41;
	CANquwr_1.can_id = can_id;
	CANquwr_1.data_adr = tran_frame;
	CANquwr_1.data_lng = 4;
	CANquwr(&CANquwr_1);
	
	return CANquwr_1.status;
}
UDINT InqSp(UDINT can_id)
{
	tran_frame[3] = 0x00;
	tran_frame[2] = 0x00;
	tran_frame[1] = 0x50;
	tran_frame[0] = 0x53;
	CANquwr_1.can_id = can_id;
	CANquwr_1.data_adr = tran_frame;
	CANquwr_1.data_lng = 4;
	CANquwr(&CANquwr_1);
	
	return CANquwr_1.status;
}
UDINT InqMo(UDINT can_id)
{
	tran_frame[3] = 0x00;
	tran_frame[2] = 0x00;
	tran_frame[1] = 0x4F;
	tran_frame[0] = 0x4D;
	CANquwr_1.can_id = can_id;
	CANquwr_1.data_adr = tran_frame;
	CANquwr_1.data_lng = 4;
	CANquwr(&CANquwr_1);
	
	return CANquwr_1.status;
}
UDINT InqUm(UDINT can_id)
{
	tran_frame[3] = 0x00;
	tran_frame[2] = 0x00;
	tran_frame[1] = 0x4D;
	tran_frame[0] = 0x55;
	CANquwr_1.can_id = can_id;
	CANquwr_1.data_adr = tran_frame;
	CANquwr_1.data_lng = 4;
	CANquwr(&CANquwr_1);
	
	return CANquwr_1.status;
}

UDINT SetMo(UDINT can_id, BOOL value)
{
	tran_frame[7] = 0x00;
	tran_frame[6] = 0x00;
	tran_frame[5] = 0x00;
	tran_frame[4] = value;
	tran_frame[3] = 0x00;
	tran_frame[2] = 0x00;
	tran_frame[1] = 0x4F;
	tran_frame[0] = 0x4D;
	CANquwr_1.can_id = can_id;
	CANquwr_1.data_adr = tran_frame;
	CANquwr_1.data_lng = 8;
	CANquwr(&CANquwr_1);
	
	return CANquwr_1.status;
}
UDINT SetUm(UDINT can_id, USINT value)
{
	if((value > 0) && (value <= 5))
	{
		tran_frame[7] = 0x00;
		tran_frame[6] = 0x00;
		tran_frame[5] = 0x00;
		tran_frame[4] = value;
		tran_frame[3] = 0x00;
		tran_frame[2] = 0x00;
		tran_frame[1] = 0x4D;
		tran_frame[0] = 0x55;
		CANquwr_1.can_id = can_id;
		CANquwr_1.data_adr = tran_frame;
		CANquwr_1.data_lng = 8;
		CANquwr(&CANquwr_1);
	
		return CANquwr_1.status;
	}
	else
	{
		return 0xffff;
	}
}
UDINT SetJv(UDINT can_id, UDINT value)
{
	UDINT value2;
	value2 = (UDINT)value;
	tran_frame[7] = value2 / 0x10000 / 0x100;
	tran_frame[6] = value2 / 0x10000 % 0x100;
	tran_frame[5] = value2 % 0x10000 / 0x100;
	tran_frame[4] = value2 % 0x10000 % 0x100;
	tran_frame[3] = 0x00;
	tran_frame[2] = 0x00;
	tran_frame[1] = 0x56;
	tran_frame[0] = 0x4A;
	CANquwr_1.can_id = can_id;
	CANquwr_1.data_adr = tran_frame;
	CANquwr_1.data_lng = 8;
	CANquwr(&CANquwr_1);
	
	return CANquwr_1.status;
}
UDINT SetPa(UDINT can_id, DINT value)
{
	UDINT value2;
	value2 = (UDINT)value;
	tran_frame[7] = value2 / 0x10000 / 0x100;
	tran_frame[6] = value2 / 0x10000 % 0x100;
	tran_frame[5] = value2 % 0x10000 / 0x100;
	tran_frame[4] = value2 % 0x10000 % 0x100;
	tran_frame[3] = 0x00;
	tran_frame[2] = 0x00;
	tran_frame[1] = 0x41;
	tran_frame[0] = 0x50;
	CANquwr_1.can_id = can_id;
	CANquwr_1.data_adr = tran_frame;
	CANquwr_1.data_lng = 8;
	CANquwr(&CANquwr_1);
	
	return CANquwr_1.status;
}
UDINT SetAc(UDINT can_id, DINT value)
{
	UDINT value2;
	value2 = (UDINT)value;
	tran_frame[7] = value2 / 0x10000 / 0x100;
	tran_frame[6] = value2 / 0x10000 % 0x100;
	tran_frame[5] = value2 % 0x10000 / 0x100;
	tran_frame[4] = value2 % 0x10000 % 0x100;
	tran_frame[3] = 0x00;
	tran_frame[2] = 0x00;
	tran_frame[1] = 0x43;
	tran_frame[0] = 0x41;
	CANquwr_1.can_id = can_id;
	CANquwr_1.data_adr = tran_frame;
	CANquwr_1.data_lng = 8;
	CANquwr(&CANquwr_1);
	
	return CANquwr_1.status;
}
UDINT SetSp(UDINT can_id, DINT value)
{
	UDINT value2;
	value2 = (UDINT)value;
	tran_frame[7] = value2 / 0x10000 / 0x100;
	tran_frame[6] = value2 / 0x10000 % 0x100;
	tran_frame[5] = value2 % 0x10000 / 0x100;
	tran_frame[4] = value2 % 0x10000 % 0x100;
	tran_frame[3] = 0x00;
	tran_frame[2] = 0x00;
	tran_frame[1] = 0x50;
	tran_frame[0] = 0x53;
	CANquwr_1.can_id = can_id;
	CANquwr_1.data_adr = tran_frame;
	CANquwr_1.data_lng = 8;
	CANquwr(&CANquwr_1);
	
	return CANquwr_1.status;
}
UDINT SetBg(UDINT can_id)
{
	tran_frame[3] = 0x00;
	tran_frame[2] = 0x00;
	tran_frame[1] = 0x47;
	tran_frame[0] = 0x42;
	CANquwr_1.can_id = can_id;
	CANquwr_1.data_adr = tran_frame;
	CANquwr_1.data_lng = 4;
	CANquwr(&CANquwr_1);
	
	return CANquwr_1.status;
}
UDINT SetSt(UDINT can_id)
{
	tran_frame[3] = 0x00;
	tran_frame[2] = 0x00;
	tran_frame[1] = 0x54;
	tran_frame[0] = 0x53;
	CANquwr_1.can_id = can_id;
	CANquwr_1.data_adr = tran_frame;
	CANquwr_1.data_lng = 4;
	CANquwr(&CANquwr_1);
	
	return CANquwr_1.status;
}