
#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

void _INIT ProgramInit(void)
{
//	CANopen_0.enable = 1;
//	CANopen_0.baud_rate = 50;
//	CANopen_0.cob_anz = 5;
//	CANopen_0.device = (UDINT)"IF7";
//	CANopen(&CANopen_0);
}

void _CYCLIC ProgramCyclic(void)
{
	if(CANrtr_0.request == 1)
	{
		CANrtr_0.enable = 1;
		CANrtr_0.us_ident = CANopen_0.us_ident;
		CANrtr_0.can_id = 0x100;
		CANrtr_0.data_adr = (UDINT)RecData;
		CANrtr_0.request = 0;
		CANrtr(&CANrtr_0);
	}
	else
	{
		CANrtr_0.enable = 1;
		CANrtr_0.us_ident = CANopen_0.us_ident;
		CANrtr_0.can_id = 0x100;
		CANrtr_0.data_adr = (UDINT)RecData;
		CANrtr_0.request = 1;
		CANrtr(&CANrtr_0);
	}
	if(CANrtr_0.status == 0)
	{
		Battery.Voltage 	 = (RecData[0] * 256 + RecData[1]) * 10;//Uint is mV
		Battery.Current 	 = ((INT)(RecData[2] * 256 + RecData[3])) * 10;//Uint is mA
		Battery.RestCapacity = (RecData[4] * 256 + RecData[5]) * 10;//Uint is mAh
		
	}
	
	if(CANrtr_1.request == 1)
	{
		CANrtr_1.enable = 1;
		CANrtr_1.us_ident = CANopen_0.us_ident;
		CANrtr_1.can_id = 0x101;
		CANrtr_1.data_adr = (UDINT)RecData;
		CANrtr_1.request = 0;
		CANrtr(&CANrtr_1);
	}
	else
	{
		CANrtr_1.enable = 1;
		CANrtr_1.us_ident = CANopen_0.us_ident;
		CANrtr_1.can_id = 0x101;
		CANrtr_1.data_adr = (UDINT)RecData;
		CANrtr_1.request = 1;
		CANrtr(&CANrtr_1);
	}
	if(CANrtr_1.status == 0)
	{
		Battery.NominalCapacity = ((UINT)RecData[0] * 256 + RecData[1]) * 10;
		Battery.CyclicCount 	= (INT)((UINT)RecData[2] * 256 + RecData[3]);
		Battery.Percentage 		= (UINT)RecData[4] * 256 + RecData[5];
	}
	
	if((Battery.Percentage<=100) && (Battery.Percentage>75))
	{
		DOMapping.PowerLed1 = 1;
		DOMapping.PowerLed2 = 1;
		DOMapping.PowerLed3 = 1;
		DOMapping.PowerLed4 = 1;
	}
	else if((Battery.Percentage<=75) && (Battery.Percentage>50))
	{
		DOMapping.PowerLed1 = 1;
		DOMapping.PowerLed2 = 1;
		DOMapping.PowerLed3 = 1;
		DOMapping.PowerLed4 = 0;
	}
	else if((Battery.Percentage<=50) && (Battery.Percentage>25))
	{
		DOMapping.PowerLed1 = 1;
		DOMapping.PowerLed2 = 1;
		DOMapping.PowerLed3 = 0;
		DOMapping.PowerLed4 = 0;
	}
	else
	{
		DOMapping.PowerLed1 = 1;
		DOMapping.PowerLed2 = 0;
		DOMapping.PowerLed3 = 0;
		DOMapping.PowerLed4 = 0;
	}	
	
}

void _EXIT ProgramExit(void)
{

}

