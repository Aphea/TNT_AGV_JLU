
#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

void _INIT ProgramInit(void)
{

}

void _CYCLIC ProgramCyclic(void)
{
	if(DrivingModel.Brake.start == 0)
	{
		DOMapping.BrakeEnable = 1;
		DOMapping.BrakeStart = 0;
		
		DrivingModel.Brake.count = 0;
	}
	else
	{
		if(DrivingModel.Brake.count < 200)	//制动1s
		{
			DOMapping.BrakeEnable = 1;
			DOMapping.BrakeStart = 1;
			DrivingModel.Brake.count ++;
		}
		else 								//超过1s，取消电机使能
		{
			DOMapping.BrakeEnable = 0;
		}
		
//		if(DrivingModel.Brake.count < 200)
//		{
//			DrivingModel.Brake.count ++;
//		}
	}
}

void _EXIT ProgramExit(void)
{

}

